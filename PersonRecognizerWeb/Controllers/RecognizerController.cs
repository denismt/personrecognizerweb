﻿using Emgu.CV;
using Emgu.CV.Face;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using PersonRecognizerWeb.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace PersonRecognizerWeb.Controllers
{
    public class RecognizerController : Controller
    {
        private FaceRecognizer _recognizer;

        public RecognizerController()
        {
            _recognizer = new LBPHFaceRecognizer(1, 8, 8, 8, 45);
            string trainedDataPath = HostingEnvironment.MapPath("~/RecognizerData/Training/trained.xml");
            if (System.IO.File.Exists(trainedDataPath))
                _recognizer.Load(trainedDataPath);
        }

        [HttpPost]
        public string Train()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            try
            {
                List<Image<Gray, Byte>> images = new List<Image<Gray, byte>>();
                List<int> ids = new List<int>();
                string rootDirectoryPath = Server.MapPath("~/RecognizerData/People");
                string[] directories = Directory.GetDirectories(rootDirectoryPath);
                foreach (string directory in directories)
                {
                    int pathSeparatorIndex = directory.LastIndexOf("\\");
                    string directoryName = directory.Substring(pathSeparatorIndex + 1, directory.Length - pathSeparatorIndex - 1);
                    //Retrieve id
                    int personId = int.Parse(directoryName);
                    string[] files = Directory.GetFiles(directory);
                    foreach (string file in files)
                    {
                        Image<Gray, Byte> image = new Image<Gray, byte>(file);
                        images.Add(image);
                        ids.Add(personId);
                    }
                }
                _recognizer.Train(images.ToArray(), ids.ToArray());
                string trainingFilePath = Server.MapPath("~/RecognizerData/Training/trained.xml");
                _recognizer.Save(trainingFilePath);
            }
            catch(Exception ex)
            {
                return JsonConvert.SerializeObject(new { exception=ex.Message, stacktrace=ex.StackTrace });
            }
            return JsonConvert.SerializeObject(new { result = "ok" });
        }

        [HttpPost]
        public string Recognize(string image_data)
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            byte[] imageBinary = Convert.FromBase64String(image_data);
            Bitmap bmp;
            using (var ms = new MemoryStream(imageBinary))
            {
                bmp = new Bitmap(ms);
            }
            Image<Gray, Byte> image = new Image<Gray, byte>(bmp);
            FaceRecognizer.PredictionResult ER = _recognizer.Predict(image);
            return JsonConvert.SerializeObject(new { result = ER.Label });
        }
    }
}